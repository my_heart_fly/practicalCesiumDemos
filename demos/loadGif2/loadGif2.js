/*
 * @Author: P.Zhao
 * @Company: HT
 * @Date: 2022-06-13 22:35:08
 * @LastEditors: P.Zhao
 * @LastEditTime: 2022-06-16 11:01:45
 * @Description: cesium加载动图方案二：通过gifler库实现
 * @FilePath: \practicalCesiumDemos\demos\loadGif2\loadGif2.js
 */

import viewer from '../../source/core/viewerInstance.js'

let point = [113.94, 23.5]

// 加载gif
loadGif()

// 定位
flyTo()

/**
 * 加载gif
 */
function loadGif() {
  let url = '../../assets/images/minions.gif'
  let gif = gifler(url)

  let entity = viewer.entities.add({
    position: Cesium.Cartesian3.fromDegrees(point[0], point[1]),
    billboard: {
      scale: 0.25
    }
  })

  // 解析gif每帧图片，按时间序列进行切换
  gif.frames(document.createElement('canvas'), function (ctx, frame) {
    entity.billboard.image = new Cesium.CallbackProperty(() => {
      return frame.buffer.toDataURL()
    }, false)
  })
}

/**
 * 定位
 */
function flyTo() {
  viewer.camera.flyTo({
    destination: Cesium.Cartesian3.fromDegrees(point[0], point[1], 1000000),
    duration: 4
  })
}
