/*
 * @Author: P.Zhao
 * @Company: HT
 * @Date: 2022-06-13 22:35:08
 * @LastEditors: P.Zhao
 * @LastEditTime: 2022-06-15 08:54:44
 * @Description:cesium 加载动图方案一:通过libgif库实现
 * @FilePath: \practicalCesiumDemos\demos\loadGif1\loadGif1.js
 */

import viewer from '../../source/core/viewerInstance.js'

let point = [113.94, 23.5]

// 加载gif
loadGif()

// 定位
flyTo()

/**
 * 加载gif
 */
function loadGif() {
  let url = '../../assets/images/minions.gif'
  let gifDiv = document.createElement('div')
  let gifImg = document.createElement('img')

  // gif库需要img标签配置下面两个属性
  gifImg.setAttribute('rel:animated_src', url)
  gifImg.setAttribute('rel:auto_play', '1') // 设置自动播放属性
  gifDiv.appendChild(gifImg)

  let superGif = new SuperGif({
    gif: gifImg
  })
  superGif.load(function () {
    viewer.entities.add({
      position: Cesium.Cartesian3.fromDegrees(point[0], point[1]),
      billboard: {
        image: new Cesium.CallbackProperty(() => {
          // 转成base64,直接加canvas理论上是可以的，这里设置有问题
          return superGif.get_canvas().toDataURL()
        }, false),
        scale: 0.25
      }
    })
  })
}

/**
 * 定位
 */
function flyTo() {
  viewer.camera.flyTo({
    destination: Cesium.Cartesian3.fromDegrees(point[0], point[1], 1000000),
    duration: 4
  })
}
