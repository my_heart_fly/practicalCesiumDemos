/*
 * @Author: P.Zhao
 * @Company: HT
 * @Date: 2022-06-13 22:35:08
 * @LastEditors: P.Zhao
 * @LastEditTime: 2022-07-01 15:47:40
 * @Description: 量测工具
 * @FilePath: \practicalCesiumDemos\demos\measure1\measure1.js
 */

import viewer from '../../source/core/viewerInstance.js'
import { MeasureTool } from '../../libs/plugins/cesium-measure-es6.js'

let point = [-117.16, 32.71, 150000.0]

// 添加影像
loadImageLayer()

// 加载地形
loaddTerrain()

// 定位
flyTo()

// 添加绘制工具
addMeasureTool()

/**
 * 定位
 */
function flyTo() {
  viewer.camera.flyTo({
    destination: Cesium.Cartesian3.fromDegrees(point[0], point[1], point[2]),
    duration: 4
  })
}

/**
 * 添加cesium自带的地形
 */
function loaddTerrain() {
  viewer.scene.terrainProvider = Cesium.createWorldTerrain()
}

/**
 * 添加影像图层
 */
function loadImageLayer() {
  var imageLayer = new Cesium.UrlTemplateImageryProvider({
    url: 'http://map.geoq.cn/ArcGIS/rest/services/ChinaOnlineStreetPurplishBlue/MapServer/tile/{z}/{y}/{x}/'
  })

  viewer.imageryLayers.addImageryProvider(imageLayer)
}

/**
 * 添加量测工具
 */
function addMeasureTool() {
  //   let measureTool = new Cesium.Measure(viewer)
  let measureTool = new MeasureTool(viewer)
  let clampToGround = true
  Sandcastle.addToolbarMenu([
    {
      text: '距离测量',
      onselect: function () {
        // 清除
        measureTool._drawLayer.entities.removeAll()

        // 连续量测
        // let drawLine = function () {
        //   measureTool.drawLineMeasureGraphics({
        //     clampToGround: clampToGround,
        //     callback: () => {
        //       drawLine()
        //     }
        //   })
        // }

        // drawLine()

        measureTool.drawLineMeasureGraphics({
          clampToGround: clampToGround,
          callback: () => {}
        })
      }
    },
    {
      text: '面积测量',
      onselect: function () {
        // 清除
        measureTool._drawLayer.entities.removeAll()
        measureTool.drawAreaMeasureGraphics({
          clampToGround: clampToGround,
          callback: () => {}
        })
      }
    },
    {
      text: '三角测量',
      onselect: function () {
        // 清除
        measureTool._drawLayer.entities.removeAll()
        measureTool.drawTrianglesMeasureGraphics({ callback: () => {} })
      }
    }
  ])

  //Sandcastle_End
  Sandcastle.finishedLoading()
}
