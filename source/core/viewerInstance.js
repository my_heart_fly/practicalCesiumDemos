/*
 * @Author: P.Zhao
 * @Company: HT
 * @Date: 2022-06-13 22:27:29
 * @LastEditors: P.Zhao
 * @LastEditTime: 2022-06-13 22:44:27
 * @Description: viewer实例
 * @FilePath: \practicalCesiumDemos\source\core\viewerInstance.js
 */

var viewer = new Cesium.Viewer('cesiumContainer', {
  imageryProvider: new Cesium.UrlTemplateImageryProvider({
    url: 'http://map.geoq.cn/ArcGIS/rest/services/ChinaOnlineStreetWarm/MapServer/tile/{z}/{y}/{x}/'
  }),
  baseLayerPicker: false, //基础影响图层选择器
  navigationHelpButton: false, //导航帮助按钮
  animation: false, //动画控件
  timeline: false, //时间控件
  shouldAnimate: true, //模型动画效果
  selectionIndicator: false, //设置绿色框框不可见
  infoBox: false, //提示框
  skyBox: false //天空盒
})

//去除版权
viewer._cesiumWidget._creditContainer.style.display = 'none'

//显示刷新率和帧率
viewer.scene.debugShowFramesPerSecond = true

//开启深度测试
// viewer.scene.globe.depthTestAgainstTerrain = true;

//设置球体的颜色
// viewer.scene.globe.baseColor=Cesium.Color.RED.withAlpha(0.1);

//将viewer添加到window属性中
window.CesiumViewer = viewer // for Debugger

export default viewer
